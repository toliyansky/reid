# REID - Readable ID

#### Official implementation: [uuid-kit.netlify.app](https://uuid-kit.netlify.app/) 
[![Netlify Status](https://api.netlify.com/api/v1/badges/9893bdfc-928f-4dbe-b982-07c8b9a89e27/deploy-status)](https://app.netlify.com/sites/uuid-kit/deploys)

#### Description

REID is set of rules that define convert process from common text to UUID format.

REID only define how convert common text to UUID format.
You must yourself to decide how to use the text id for place readable information in UUID.

When is it needed?<br>
Sometimes you have a small set of records with a UUID identifier, and you want to make the identifiers more readable for debugging purposes or for test data.

Using examples:<br>
```c1acc000-0000-0000-0000-000000000001``` - that mean some "class" with id "1" on the end.<br>
```1ocaf10b-0003-0b9e-0005-f1e1d0000008``` - that mean some "location" with id "3", some "object" with id "5", some "field" with id 8.<br>

If you use REID as UUID in your database, you can write in REID information that describe value of many filed in row.

It can be helpful when you create difficult SQL queries with many joins, with REID you can check trueness of joins in query while you make test run of query.

If you provide to frontend only set of UUID's of object, it will difficult to understand that backend was correct made selection with all filters... 
But with REID it can be very easy.


### Convert rules

- Digits - write as is.
```[0,1,2,3,4,5,6,7,8,9]```
- Letters - six letters latin alphabet write as is. 
```[Aa,Bb,Cc,Dd,Ee,Ff]```

### Char mapping for small letters
|Small char|Reid char|
|:---:|:---:|
|g|9|
|h|b|
|i|1|
|j|1|
|k|c|
|l|1|
|m|1|
|n|b|
|o|0|
|p|f|
|q|9|
|r|f|
|s|3|
|t|f|
|u|c|
|v|7|
|w|d|
|x|f|
|y|9|
|z|7|

### Char mapping for capital letter
|Capital char|Reid char|
|:---:|:---:|
|G|6|
|H|E|
|I|1|
|J|9|
|K|E|
|L|1|
|M|A|
|N|D|
|O|0|
|P|B|
|Q|0|
|R|B|
|S|B|
|T|F|
|U|D|
|V|7|
|W|D|
|X|8|
|Y|9|
|Z|7|

This is draft of mapping. You can propose your version in issues.<br>
Implementations may has override mapping of char tables.