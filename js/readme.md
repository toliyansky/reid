### How to use

Most likely there is no need to use the REID implementation in your project, because usually the REID is used by a human, not a machine. 
But if you want to make your own website with a converter, possibly with additional functions, you can use the 'raid.js' file as a ready-made conversion implementation.

1 Copy [reid.js](reid.js) as module in your project.

2 Add
```javascript
import {REID} from "./reid.js";
```
3 Use
```javascript
let uuid = REID.from("user-input-text");
```