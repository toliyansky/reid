// REID JS Implementation

const UUID_TEXT_LENGTH = 32;

const charMap = {
    "0": "0",
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",

    "a": "a",
    "b": "b",
    "c": "c",
    "d": "d",
    "e": "e",
    "f": "f",
    "g": "9",
    "h": "b",
    "i": "1",
    "j": "1",
    "k": "c",
    "l": "1",
    "m": "1",
    "n": "b",
    "o": "0",
    "p": "f",
    "q": "9",
    "r": "f",
    "s": "3",
    "t": "f",
    "u": "c",
    "v": "7",
    "w": "d",
    "x": "f",
    "y": "9",
    "z": "7",

    "A": "A",
    "B": "B",
    "C": "C",
    "D": "D",
    "E": "E",
    "F": "F",
    "G": "6",
    "H": "E",
    "I": "1",
    "J": "9",
    "K": "E",
    "L": "1",
    "M": "A",
    "N": "D",
    "O": "0",
    "P": "B",
    "Q": "0",
    "R": "B",
    "S": "B",
    "T": "F",
    "U": "D",
    "V": "7",
    "W": "D",
    "X": "8",
    "Y": "9",
    "Z": "7"
};

function ReadableId() {
}

ReadableId.prototype.from = function (text) {
    if (typeof text !== 'string') {
        throw new Error('Input argument must bee string type.');
    }
    if (text.length < UUID_TEXT_LENGTH) {
        throw new Error(`Text '${text}' length less than UUID length.`);
    }
    if (text.length > UUID_TEXT_LENGTH) {
        this.log(`Text '${text}' length more than UUID length. Try remove dashes.`);
        text = this.removeDashesFromString(text);
        if (text.length !== UUID_TEXT_LENGTH) {
            throw new Error(`Text '${text}' length after remove dashes not correct.`);
        }
    }
    let result = "";
    for (let i = 0; i < text.length; i++) {
        let currentChar = text.charAt(i);
        let mappedChar = charMap[currentChar];
        if (typeof mappedChar === 'undefined') {
            throw new Error(`Symbol '${currentChar}' can't be mapped into UUID. It will cut.`);
        }
        result += mappedChar;
    }
    if (result.length < UUID_TEXT_LENGTH) {
        throw new Error(`Error. Final string length less UUID length. Result: ${result}.`);
    }
    this.log(`Result: ${result}`);
    return result;
}

ReadableId.prototype.removeDashesFromString = function (text) {
    this.log(`Remove dashes from ${text}`);
    return text.replace(`-`, text)
}

ReadableId.prototype.log = function (text) {
    console.log(`[REID DEBUG] ${text}`);
}

let REID = new ReadableId();
REID.UUID_TEXT_LENGTH = UUID_TEXT_LENGTH;
REID.charMap = charMap;
window.REID = REID;

export {REID};