import {REID} from "../reid.js";

console.log(`Hello. If you want use REID function from console - you are welcome.
Example: REID.from('Hello000000000000000000000000000')
Be carefully with text length and accessible symbols, 
because clean reid.js required correct input and all additional checks and auto fillings on this site not described in specification.`);

let inputText = document.getElementById('inputText');
let outputText = document.getElementById('outputText');
let settingsButton = document.getElementById('settingsButton');
let myToast = document.getElementById('myToast');
let copyPopover = document.getElementById('copyPopover');
let lastNotificationTime = Date.now();
let hideFirstTimeDialogButton = null;
let showGuideButton = null;

let defaultSettings = {
    isResultUuidWithDashes: true,
    isUpperCaseInput: false,
    isUpperCaseResult: false,
    isResultWithBrackets: false,
    isAutoCoping: true,
    isFirstTime: true
};

let settings = {};
settings.isResultUuidWithDashes = localStorage.getItem('resultUuidWithDashesSwitch') === 'true';
settings.isUpperCaseInput = localStorage.getItem('isUpperCaseInput') === 'true';
settings.isUpperCaseResult = localStorage.getItem('isUpperCaseResult') === 'true';
settings.isResultWithBrackets = localStorage.getItem('isResultWithBrackets') === 'true';
settings.isAutoCoping = localStorage.getItem('isAutoCoping') === 'true';
let isFirstTime = settings.isFirstTime = localStorage.getItem('isFirstTime');
settings.isFirstTime = isFirstTime == null;

outputText.onclick = function () {
    if (settings.isAutoCoping && outputText.value.length > 0) {
        navigator.clipboard.writeText(outputText.value).then(() => {
            copyPopover.show(this).then(function () {
                setTimeout(function () {
                    copyPopover.hide();
                }, 300);
            });
        });
    }
}

inputText.oninput = function () {
    let input = inputText.value;

    if (input.length === 0) {
        outputText.value = '';
        return;
    }

    // Remove unexpected symbols
    let tempText = '';
    for (let i = 0; i < input.length; i++) {
        let currentSymbol = input.charAt(i);
        if (currentSymbol === '-') {
            // '-' No in charMap, but we accept '-' for user on input
        } else if (typeof REID.charMap[currentSymbol] === 'undefined') {
            console.log(`Remove symbol ${currentSymbol} because it can't be mapped`);
            // Notification anti spam
            if (Date.now() > lastNotificationTime + 500) {
                lastNotificationTime = Date.now();
                myToast.show().then(function () {
                    setTimeout(function () {
                        myToast.hide();
                    }, 500);
                });
            }
            continue;
        }
        tempText += currentSymbol;
    }
    input = tempText;

    if (settings.isUpperCaseInput) {
        input = input.toUpperCase();
    }

    inputText.value = input;

    input = input.replace(/-/g, '');

    // If text length less than 32, then add '0' while text length will equal 32
    while (input.length < REID.UUID_TEXT_LENGTH) {
        input += '0';
    }
    // If text length more than 32, then cut to 32.
    if (input.length > REID.UUID_TEXT_LENGTH) {
        input = input.substring(0, REID.UUID_TEXT_LENGTH);
    }

    try {
        let result = REID.from(input);

        if (settings.isResultUuidWithDashes) {
            result = result.substring(0, 8) + '-' +
                result.substring(8, 12) + '-' +
                result.substring(12, 16) + '-' +
                result.substring(16, 20) + '-' +
                result.substring(20, 32)
        }

        if (settings.isUpperCaseResult) {
            result = result.toUpperCase();
        }

        if (settings.isResultWithBrackets) {
            result = `{${result}}`;
        }

        outputText.value = result;
    } catch (e) {
        outputText.value = '';
    }
}

let hideSettingsDialog = function () {
    document.getElementById('settings-dialog').hide();
};

let showSettingsDialog = function () {
    let dialog = document.getElementById('settings-dialog');

    if (dialog) {
        dialog.show();
    } else {
        ons.createElement('settings-dialog.html', {append: true})
            .then(function (dialog) {
                dialog.show();
                let hideSettingsButton = document.getElementById('hideSettingsButton');
                hideSettingsButton.onclick = hideSettingsDialog;

                let resultUuidWithDashesSwitch = document.getElementById('resultUuidWithDashesSwitch');
                resultUuidWithDashesSwitch.checked = settings.isResultUuidWithDashes;
                resultUuidWithDashesSwitch.onchange = function () {
                    settings.isResultUuidWithDashes = resultUuidWithDashesSwitch.checked;
                    localStorage.setItem('resultUuidWithDashesSwitch', settings.isResultUuidWithDashes);
                    inputText.oninput(null);
                }

                let upperCaseInputSwitch = document.getElementById('upperCaseInputSwitch');
                upperCaseInputSwitch.checked = settings.isUpperCaseInput;
                upperCaseInputSwitch.onchange = function () {
                    settings.isUpperCaseInput = upperCaseInputSwitch.checked;
                    localStorage.setItem('isUpperCaseInput', settings.isUpperCaseInput);
                    inputText.oninput(null);
                }

                let upperCaseResultSwitch = document.getElementById('upperCaseResultSwitch');
                upperCaseResultSwitch.checked = settings.isUpperCaseResult;
                upperCaseResultSwitch.onchange = function () {
                    settings.isUpperCaseResult = upperCaseResultSwitch.checked;
                    localStorage.setItem('isUpperCaseResult', settings.isUpperCaseResult);
                    inputText.oninput(null);
                }

                let resultWithBracketsSwitch = document.getElementById('resultWithBracketsSwitch');
                resultWithBracketsSwitch.checked = settings.isResultWithBrackets;
                resultWithBracketsSwitch.onchange = function () {
                    settings.isResultWithBrackets = resultWithBracketsSwitch.checked;
                    localStorage.setItem('isResultWithBrackets', settings.isResultWithBrackets);
                    inputText.oninput(null);
                }

                let autoCopingSwitch = document.getElementById('autoCopingSwitch');
                autoCopingSwitch.checked = settings.isAutoCoping;
                autoCopingSwitch.onchange = function () {
                    settings.isAutoCoping = autoCopingSwitch.checked;
                    localStorage.setItem('isAutoCoping', settings.isAutoCoping);
                    inputText.oninput(null);
                }
            });
    }
};

let hideFirstTimeDialog = function () {
    settings.isFirstTime = false;
    localStorage.setItem('isFirstTime', settings.isFirstTime);
    document.getElementById('first-time-dialog').hide();
}

let hideGuide = function () {
    document.getElementById('guide').hide();
}

let showGuide = function () {
    hideFirstTimeDialog();
    ons.createElement('guide.html', {append: true})
        .then(function (dialog) {
            dialog.show();
            hideFirstTimeDialogButton = document.getElementById('guide');
            hideFirstTimeDialogButton.onclick = hideFirstTimeDialog;
            document.getElementById('hideGuideButton').onclick = hideGuide;
        });
}

let showFirstTimeDialog = function () {
    let dialog = document.getElementById('first-time-dialog');

    if (dialog) {
        dialog.show();
        hideFirstTimeDialogButton = document.getElementById('hideFirstTimeDialogButton');
        hideFirstTimeDialogButton.onclick = hideFirstTimeDialog;
        showGuideButton = document.getElementById('showGuideButton');
        showGuideButton.onclick = showGuide;
    } else {
        ons.createElement('first-time-dialog.html', {append: true})
            .then(function (dialog) {
                dialog.show();
                hideFirstTimeDialogButton = document.getElementById('hideFirstTimeDialogButton');
                hideFirstTimeDialogButton.onclick = hideFirstTimeDialog;
                showGuideButton = document.getElementById('showGuideButton');
                showGuideButton.onclick = showGuide;
            });
    }
};

if (settings.isFirstTime) {
    console.log("User come first time. Set default settings.");
    settings = defaultSettings;
    for (let param in settings) {
        localStorage.setItem(param, settings[param]);
    }
    showFirstTimeDialog();
}

settingsButton.onclick = showSettingsDialog;